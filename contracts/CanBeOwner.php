<?php

/*
 * This file is part of Laravel Ownership.
 *
 * (c) Ivan Bakran <ivan@nsid.hr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nsid\Contracts\Ownership;

/**
 * Interface CanBeOwner.
 *
 * @package Nsid\Contracts\Ownership
 */
interface CanBeOwner
{
    /**
     * Get the value of the model's primary key.
     *
     * @return mixed
     */
    public function getKey();

    /**
     * Get the class name for polymorphic relations.
     *
     * @return string
     */
    public function getMorphClass();
}
