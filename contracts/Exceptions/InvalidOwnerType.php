<?php

/*
 * This file is part of Laravel Ownership.
 *
 * (c) Ivan Bakran <ivan@nsid.hr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nsid\Contracts\Ownership\Exceptions;

use Nsid\Contracts\Ownership\CanBeOwner as CanBeOwnerContract;
use Nsid\Contracts\Ownership\Ownable as OwnableContract;
use Exception;

/**
 * Class InvalidOwnerType.
 *
 * @package Nsid\Contracts\Ownership\Exceptions
 */
class InvalidOwnerType extends Exception
{
    /**
     * Owner of the provided type is not allowed to own this model.
     *
     * @param \Nsid\Contracts\Ownership\Ownable $ownable
     * @param \Nsid\Contracts\Ownership\CanBeOwner $owner
     * @return static
     */
    public static function notAllowed(OwnableContract $ownable, CanBeOwnerContract $owner)
    {
        return new static(sprintf('Model `%s` not allows owner of type `%s`.', get_class($ownable), get_class($owner)));
    }
}
