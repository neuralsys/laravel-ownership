<?php

/*
 * This file is part of Laravel Ownership.
 *
 * (c) Ivan Bakran <ivan@nsid.hr>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nsid\Laravel\Ownership\Observers;

use Nsid\Contracts\Ownership\Ownable as OwnableContract;

/**
 * Class OwnableObserver.
 *
 * @package Nsid\Laravel\Ownership\Observers
 */
class OwnableObserver
{
    /**
     * Handle the deleted event for the model.
     *
     * @param \Nsid\Contracts\Ownership\Ownable $ownable
     * @return void
     */
    public function creating(OwnableContract $ownable)
    {
        if ($ownable->isDefaultOwnerOnCreateRequired()) {
            $ownable->withDefaultOwner();
        }

        if ($owner = $ownable->defaultOwner()) {
            $ownable->changeOwnerTo($owner);
        }
    }
}
